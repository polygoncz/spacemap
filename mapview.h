#pragma once

#include <QGraphicsView>
#include <QList>

#include "graphics.h"

class MapMainWindow;

class MapView : public QGraphicsView
{
    Q_OBJECT;
public:
    explicit MapView(QWidget* parent = nullptr, MapMainWindow* wnd = nullptr);
    ~MapView();

#ifndef QT_NO_WHEELEVENT
    virtual void wheelEvent(QWheelEvent* ev) override;
#endif

    void addNode(Node* n);
    void addEdge(Edge* e);
    void clear(void);

    Node* selectedNode(void) const;
    Edge* selectedEdge(void) const;
    int selectedItemType(void) const;
    bool hasSelectedItem(void) const;

    QSize sceneSize(void) const;

protected:
    void mousePressEvent(QMouseEvent* ev) override;
    void mouseReleaseEvent(QMouseEvent* event) override;

public slots:
    void zoomIn();
    void zoomOut();

signals:
    void nodeAdded(Node*);
    void edgeAdded(Edge*);

private:
    QGraphicsScene* _scene;
    QGraphicsPixmapItem* background;
    MapMainWindow* mainwindow;

    QList<Node *> nodes;
    QList<Edge *> edges;

    Node* fromNode;
    bool creatingEdge;

    double currentZoomFactor;
    static const double stepScaleFactor;
};

