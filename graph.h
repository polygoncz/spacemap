﻿#pragma once

#include <map>
#include <vector>
#include <functional>
#include <stdexcept>

namespace SpaceMapEditor
{
namespace Structures
{

/*!
* \brief Data structure of unoriented graph.
*
* Graph uses for storing nodes as key in `std::map` STL container.
* Value is second one `std::map` container which contains target node as key
* and edge type as value. Avoid using pointers as node or edge type.
* You must check deallocating of it. Use a `shared_ptr` instead it.
*
* Complexicity:
* -------------
*
* Operation      | Complexicity
* ---------      | ------------
* node insertion | \f$ O(\log(n)) \f$
* edge insertion | \f$ O(\log(n)) \f$
* edge delete    | \f$ O(\log(n)) \f$
* find node      | \f$ O(n) \f$
* find edge      | \f$ O(\log(n)) \f$ resp. \f$ O(2n + \log(n)) \f$
*
* \tparam N node type
* \tparam E edge value type
* \tparam Comparer node comparer
*/
template <typename N, typename E, typename Comparer = std::less<N>>
class Graph
{
public:
    /*!
     * Neighbors node with connecting edges.
     */
    typedef std::vector<std::pair<const N, const E>> NeighborsList;

    /*!
     * Vector of nodes.
     */
    typedef std::vector<N> NodesList;

    /*!
     * Vector of tuples in format `<from, to, edge>`
     */
    typedef std::vector<std::tuple<const N, const N, const E>> EdgesList;

    /*!
    * Contructor of empty unoriented graph object.
    */
    Graph(void);

    /*!
    * Destructor of unoriented graph object. Beware if pointers is used as node type,
    * pointers will not be deallocated. You must deallocate it manualy.
    * `std::shared_ptr` may help you.
    */
    ~Graph(void);

    /*!
    * Clear graph structures. Do not deallocate pointers.
    */
    void clear(void);

    /*!
    * Check if structure is empty.
    * \return true when graph contains no node, false in other cases
    */
    bool empty(void) const;

    /*!
    * Returns count of nodes.
    * \return count of nodes
    */
    size_t size(void) const;

    size_t sizeEdges(void) const;

    /*!
    * Add new node into structure.
    * \param[in] node new inserted node
    * \exception std::invalid_argument when inserted node which is already in graph
    */
    void addNode(const N& node) throw(std::invalid_argument);

    /*!
    * Add new edge into structure. `From` and `to` node is switchable,
    * graph is unoriented. This method create in fact two edges one `from->to`
    * and second `to->from`.
    * \param[in] value edge item
    * \param[in] from start node
    * \param[in] to finish node
    * \exception std::out_of_range when `from` or `to` is not found
    */
    void addEdge(const E& value, const N& from, const N& to) throw(std::out_of_range);

    /*!
    * Delete edge from structure. `From` and to is switchable, graph is unoriented.
    * This method delete in fact two edges, `from->to` and `to->from`.
    * \param[in] from start node
    * \param[in] to finish node
    * \exception std::out_of_range when `from` or `to` node is not found
    */
    void deleteEdge(const N& from, const N& to) throw(std::out_of_range);

    /*!
    * Find node in graph based on comparsion with id.
    * \tparam Rhs type of id object
    * \param[in] id node identified object
    * \param[in] comp compare function (or functor, lambda) between node and Rhs object
    * \exception std::out_of_range when node was not found
    */
    template <typename Rhs>
    N findNode(const Rhs& id, std::function<bool(const N&, const Rhs&)> comp) const throw(std::out_of_range);

    /*!
    * Find edges between two nodes.
    * \param[in] from start node
    * \param[in] to finish node
    * \exception std::out_of_range when `from` or `to` node was not found
    */
    E findEdge(const N& from, const N& to) const throw(std::out_of_range);

    /*!
    * Find edges between two nodes based on identified object comparsion.
    * \tparam Rhs idetified object type
    * \param[in] from identified object of start node
    * \param[in] to indentified object of finish node
    * \param[in] comp compare function (or functor, lambda) between node and Rhs object
    * \exception std::out_of_range when `from` or `to` node was not found
    */
    template <typename Rhs>
    E findEdge(const Rhs& from, const Rhs& to, std::function<bool(const N&, const Rhs&)> comp) throw(std::out_of_range);

    /*!
    * Get neighbors node and edge pairs. Pair is provided by `std::pair` class.
    * Data is returned into out parameters.
    * \param[in] node base node
    * \param[out] out `std::vector` reference for filling with pair of edge value and finish node
    * \exception std::out_of_range when base node not found
    */
    void neighbors(const N& node, NeighborsList& out) const throw(std::out_of_range);

    /*!
    * Get neighbors node and edge pairs. Pair is provided by `std::pair` class.
    * Return `std::vector` of pairs.
    * \param[in] node base node
    * \return `std::vector` of pairs
    * \exception std::out_of_range when base node not found
    */
    NeighborsList neighbors(const N& node) const throw(std::out_of_range);

    /*!
    * Get all nodes in graph into `std::list`. There are no duplicites.
    * \param[out] out reference to `std::list` for filling
    */
    void nodes(NodesList& out) const;

    /*!
    * Get all nodes in graph into `std::list`. There are no duplicites.
    * \return `std::vector` of nodes objects
    */
    NodesList nodes() const;

    /*!
    * Get all edges in graph into `std::list`. The data about nodes and edge value
    * are stored in `std::tuple<N, N, E>` like `<from, to, value>`. There are no duplicites.
    * Do not use this operation often. It is expensive operation, because there is
    * duplicites check.
    * \param[out] out reference to `std::vector` for filling
    */
    void edges(EdgesList& out) const;

    /*!
    * Get all edges in graph into `std::list`. The data about nodes and edge value
    * are stored in `std::tuple<N, N, E>` like `<from, to, value>`. There are no duplicites.
    * Do not use this operation often. It is expensive operation, because there is
    * duplicites check.
    * \return `std::vector` of edges without duplicites
    */
    EdgesList edges() const;

private:
    std::map<N, std::map<N, E, Comparer>, Comparer> matrix; //!< matrix of incidence
    size_t edgesCount; //!< count of edges
};

template <typename N, typename E, typename Comparer>
Graph<N, E, Comparer>::Graph(void)
{
    edgesCount = 0;
}

template <typename N, typename E, typename Comparer>
Graph<N, E, Comparer>::~Graph(void)
{
}

template <typename N, typename E, typename Comparer>
void Graph<N, E, Comparer>::clear(void)
{
    matrix.clear();
}

template <typename N, typename E, typename Comparer>
bool Graph<N, E, Comparer>::empty(void) const
{
    return matrix.empty();
}

template <typename N, typename E, typename Comparer>
size_t Graph<N, E, Comparer>::size(void) const
{
    return matrix.size();
}

template <typename N, typename E, typename Comparer>
size_t Graph<N, E, Comparer>::sizeEdges() const
{
    return edgesCount;
}

template <typename N, typename E, typename Comparer>
void Graph<N, E, Comparer>::addNode(const N& node) throw(std::invalid_argument)
{
    if (matrix.count(node) == 0)
        matrix.insert(std::make_pair(node, std::map<N, E, Comparer>()));
    else
        throw std::invalid_argument("Inserted node is already in graph.");
}

template <typename N, typename E, typename Comparer>
void Graph<N, E, Comparer>::addEdge(const E& value, const N& from, const N& to)
{
    using std::map;

    map<N, E>& edges1 = matrix.at(from);
    edges1.insert(std::make_pair(to, value));

    map<N, E>& edges2 = matrix.at(to);
    edges2.insert(std::make_pair(from, value));
    ++edgesCount;
}

template <typename N, typename E, typename Comparer>
void Graph<N, E, Comparer>::deleteEdge(const N& from, const N& to)
{
    using std::map;

    map<N, E>& edges1 = matrix.at(from);
    edges1.erase(to);

    map<N, E>& edges2 = matrix.at(to);
    edges2.erase(from);

    --edgesCount;
}

template <typename N, typename E, typename Comparer>
template <typename Rhs>
N Graph<N, E, Comparer>::findNode(const Rhs& id, std::function<bool(const N&, const Rhs&)> comp) const
{
    for (auto it : matrix)
    {
        const N& node = it.first;
        if (comp(node, id)) return node;
    }

    throw std::out_of_range("Node not found.");
}

template <typename N, typename E, typename Comparer>
E Graph<N, E, Comparer>::findEdge(const N& from, const N& to) const
{
    std::map<N, E> edges = matrix.at(from);
    return edges.at(to);
}

template <typename N, typename E, typename Comparer>
template <typename Rhs>
E Graph<N, E, Comparer>::findEdge(const Rhs& from, const Rhs& to, std::function<bool(const N&, const Rhs&)> comp)
{
    const N& f = findNode(from, comp);
    const N& t = findNode(to, comp);

    return findEdge(f, t);
}

template <typename N, typename E, typename Comparer>
void Graph<N, E, Comparer>::neighbors(const N& node, NeighborsList& out) const throw(std::out_of_range)
{
    using std::map;

    map<N, E> edges = matrix.at(node);
    for (auto it : edges)
        out.push_back(std::make_pair(it.first, it.second));
}

template <typename N, typename E, typename Comparer>
typename Graph<N, E, Comparer>::NeighborsList
Graph<N, E, Comparer>::neighbors(const N& node) const
{
    NeighborsList out;
    neighbors(node, out);
    return out;
}

template <typename N, typename E, typename Comparer>
void Graph<N, E, Comparer>::nodes(NodesList& out) const
{
    for (auto&& it : matrix)
        out.push_back(it.first);
}

template <typename N, typename E, typename Comparer>
typename Graph<N, E, Comparer>::NodesList Graph<N, E, Comparer>::nodes() const
{
    NodesList out;
    nodes(out);
    return out;
}

template <typename N, typename E, typename Comparer>
typename Graph<N, E, Comparer>::EdgesList Graph<N, E, Comparer>::edges() const
{
    EdgesList out;
    edges(out);
    return out;
}

template <typename N, typename E, typename Comparer>
void Graph<N, E, Comparer>::edges(EdgesList& out) const
{
    using std::make_tuple;

    for (auto&& i : matrix) //iterate over nodes
    {
        const N& from = i.first;
        const std::map<N, E>& edges = i.second;
        for (auto&& j : edges) //iterate over edges
        {
            const N& to = j.first;
            const E& edge = j.second;

            bool isDuplicate = false;
            for (auto&& tuple : out) //duplicity check
            {
                const N& from2 = std::get<0>(tuple);
                const N& to2 = std::get<1>(tuple);
                if (from2 == to && to2 == from)
                {
                    isDuplicate = true;
                    break;
                }
            }

            if (!isDuplicate) //add to out list
            {
                auto newTuple = make_tuple(from, to, edge);
                out.push_back(newTuple);
            }
        }
    }
}
}
}
