#include "stdafx.h"
#include "dijkstra.h"

#include <algorithm>
#include <limits>
#include <set>
#include <utility>
#include <vector>

using SpaceMapEditor::Structures::Graph;
using std::map;

DijkstraToNode::DijkstraToNode(Graph<Node*, Edge*>* graph,
                               Node* to)
    : graph(graph),
      to(to)
{}

int DijkstraToNode::findDistance(Node* node, std::vector<std::pair<Node*, int>> distances) const
{
    for (auto it : distances)
        if (it.first == node) return it.second;
}

map<Node*, Node*> DijkstraToNode::operator()() const
{
    using std::vector;
    using std::set;
    using std::numeric_limits;
    using std::pair;
    using std::make_pair;

    if (!graph || !to) return map<Node*,Node*>();

    vector<pair<Node*, int> > distances;
    set<Node*> closed;
    map<Node*, Node*> unaryTree;

    distances.push_back(make_pair(to, 0));

    auto nodes = graph->nodes();
    for (auto node : nodes)
    {
        if (node != to)
        {
            distances.push_back(make_pair(node, numeric_limits<int>::max()));
            unaryTree.insert(make_pair(node, nullptr));
        }
    }

    while (!distances.empty())
    {
        Node* node = distances.front().first;
        int distance = distances.front().second;
        distances.erase(distances.begin());
        closed.insert(node);

        auto neighbors = graph->neighbors(node);
        for (auto it : neighbors)
        {
            Node* n = it.first;
            Edge* edge = it.second;

            if (closed.count(n) == 0 && edge->valid())
            {
                int oldDistance = findDistance(n, distances);
                if (distance + edge->cost() < oldDistance)
                {
                    unaryTree[n] = node;

                    for (auto&& itt : distances)
                    {
                        if (itt.first == n)
                        {
                            itt.second = distance + edge->cost();
                            break;
                        }
                    }
                }
            }
        }

        std::sort(distances.begin(),
                  distances.end(),
                  [] (pair<Node*, int> a, pair<Node*, int> b) -> bool { return a.second < b.second; });
    }

    return unaryTree;
}

DijkstraFromNode::DijkstraFromNode(SpaceMapEditor::Structures::Graph<Node*, Edge*>* graph,
                                   Node* from)
    : graph(graph),
      from(from)
{ }

map<Node*, map<Node*, Node*>> DijkstraFromNode::operator()() const
{
    using std::vector;

    DijkstraToNode toNode(graph, from);
    map<Node*, Node*> followersToNode = toNode();

    map<Node*, map<Node*, Node*>> matrix;

    for (auto n : followersToNode)
    {
        vector<Node*> path;
        Node* currentNode = n.first;

        while (currentNode != from || !currentNode)
        {
            path.push_back(currentNode);
            currentNode = followersToNode[currentNode];
        }

        if (currentNode)
        {
            path.push_back(from);
            std::reverse(path.begin(), path.end());

            for (int i = 0; i < path.size() - 1; ++i)
            {
                matrix[path[i]][path[i + 1]] = path[i + 1];
            }
        }
    }

    return matrix;
}