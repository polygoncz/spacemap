#include "stdafx.h"
#include "mapmainwindow.h"
#include "dijkstra.h"

#include <QPrinter>
#include <QPrintDialog>
#include <QPrintPreviewDialog>

#include "GridSearchDialog.h"

MapMainWindow::MapMainWindow(QWidget* parent, Qt::WindowFlags flags)
    : QMainWindow(parent, flags)
{
    showComputeResult = false;
    setup();
}

MapMainWindow::~MapMainWindow()
{
}

void MapMainWindow::retranslate()
{
    planetNameLabel->setText(QApplication::translate("Form", "Planet name:", 0));
    planetColorLabel->setText(QApplication::translate("Form", "Planet color:", 0));
    colorSelectionButton->setText(QApplication::translate("Form", "...", 0));
    coordinatesLabel->setText(QApplication::translate("Form", "Coordinates:", 0));
    xLabel->setText(QApplication::translate("Form", "x:", 0));
    yLabel->setText(QApplication::translate("Form", "y:", 0));
    tabWidget->setTabText(tabWidget->indexOf(planetTab), QApplication::translate("Form", "Planet", 0));
    travelTimeLabel->setText(QApplication::translate("Form", "Travel time:", 0));
    safeLabel->setText(QApplication::translate("Form", "Safe:", 0));
    tabWidget->setTabText(tabWidget->indexOf(pathTab), QApplication::translate("Form", "Path", 0));
}

void MapMainWindow::addNode(Node* n)
{
    try
    {
        graph.addNode(n);
    }
    catch (std::invalid_argument& ex)
    {
        QMessageBox::warning(this, "Chyba vkl�d�n�!", "Uzel se stejn�m jm�nem ji� existuje.");
    }
}

void MapMainWindow::addEdge(Edge* e)
{
    graph.addEdge(e, e->from(), e->to());
}

void MapMainWindow::saveFile()
{
    using std::get;

    QString filePath = QFileDialog::getSaveFileName(this,
                       "Save map...",
                       "",
                       "Map file (*.map)");

    if (filePath.isNull()) return;

    QFile file(filePath);
    file.open(QIODevice::WriteOnly);
    QDataStream out(&file);

    int nodesCount = graph.size();
    out << nodesCount;

    auto nodes = graph.nodes();
    for (auto node : nodes)
    {
        out << node;
    }

    auto edges = graph.edges();
    int edgesCount = graph.sizeEdges();
    out << edgesCount;
    for (auto edge : edges)
    {
        out << get<2>(edge);
    }

    file.close();
}

void MapMainWindow::openFile()
{
    QString filePath = QFileDialog::getOpenFileName(this,
                       "Open map...",
                       "",
                       "Map file (*.map)");

    if (filePath.isNull()) return;

    mapView->clear();
    graph.clear();
    delete grid;
    grid = nullptr;

    QFile file(filePath);
    file.open(QIODevice::ReadOnly);

    QDataStream in(&file);
    int nodesCount;
    in >> nodesCount;

    for (int i = 0; i < nodesCount; ++i)
    {
        Node* n = new Node("temp");
        in >> n;

        graph.addNode(n);
        mapView->addNode(n);
    }

    int edgesCount;
    in >> edgesCount;
    for (int i = 0; i < edgesCount; ++i)
    {
        int value;
        QString fromS;
        QString toS;
        in >> value >> fromS >> toS;

        auto comp = [](Node* const& n, const QString& s) ->bool
        {
            return n->name() == s;
        };
        Node* from = graph.findNode<QString>(fromS, comp);
        Node* to = graph.findNode<QString>(toS, comp);
        Edge* e = new Edge(from, to, value);

        graph.addEdge(e, from, to);
        mapView->addEdge(e);
    }

    file.close();
}

void MapMainWindow::computePathsTo()
{
    Node* toNode = mapView->selectedNode();
    if (!toNode)
    {
        QMessageBox::critical(this,
                              "Planet is not selected!",
                              "You must select planet.");
        return;
    }

    DijkstraToNode algorithm(&graph, toNode);
    auto unaryTree = algorithm();

    setAllEdges(QColor(Qt::darkGray).darker(), Edge::ArrowType::None);

    for (auto path : unaryTree)
    {
        Edge* edge = graph.findEdge(path.first, path.second);
        edge->setColor(QColor(123, 247, 125));
        if (edge->from() == path.second)
            edge->setArrowType(Edge::ArrowType::From);
        else
            edge->setArrowType(Edge::ArrowType::To);
    }

    showComputeResult = true;
}

void MapMainWindow::computePathsFrom()
{
    Node* fromNode = mapView->selectedNode();
    if (!fromNode)
    {
        QMessageBox::critical(this,
                              "Planet is not selected!",
                              "You must select planet.");
        return;
    }

    setAllEdges(QColor(Qt::darkGray).darker(), Edge::ArrowType::None);

    DijkstraFromNode algorithm(&graph, fromNode);
    auto matrix = algorithm();
    for (auto row : matrix)
    {
        Node* from = row.first;
        for (auto cell : row.second)
        {
            Node* to = cell.second;

            Edge* edge = graph.findEdge(from, to);
            edge->setColor(QColor(227, 161, 223));
            if (edge->from() == from)
                edge->setArrowType(Edge::ArrowType::To);
            else
                edge->setArrowType(Edge::ArrowType::From);
        }
    }

    showComputeResult = true;
}

void MapMainWindow::setAllEdges(const QColor& color, Edge::ArrowType arrow)
{
    for (auto tuple : graph.edges())
    {
        Edge* e = std::get<2>(tuple);
        e->setColor(color);
        e->setArrowType(arrow);
    }
}

void MapMainWindow::printDocument()
{

}

void MapMainWindow::printPreview()
{
    QPrinter printer(QPrinter::HighResolution);
    printer.setOutputFormat(QPrinter::PdfFormat);
    printer.setOutputFileName("F:/test.pdf");

    QPrintPreviewDialog previewDialog(&printer);
    previewDialog.setWindowFlags(Qt::Window);
    connect(&previewDialog, &QPrintPreviewDialog::paintRequested,
            this, &MapMainWindow::preparePreview);
    previewDialog.exec();
}

void MapMainWindow::preparePreview(QPrinter* printer)
{
    QPainter painter;

    printer->setPageOrientation(QPageLayout::Landscape);

    painter.begin(printer);
    QGraphicsScene* scene = mapView->scene();
    scene->render(&painter);

    printer->setPageOrientation(QPageLayout::Portrait);
    printer->newPage();
    scene->render(&painter);

    painter.end();
}

void MapMainWindow::selectionChanged()
{
    selectedEdge = nullptr;
    selectedNode = nullptr;

    setEdgeTabEnabled(false);
    setNodeTabEnabled(false);

    if (mapView->hasSelectedItem() && showComputeResult)
    {
        setAllEdges(QColor(126, 202, 234), Edge::None);
        showComputeResult = false;
    }

    if (mapView->hasSelectedItem())
    {
        switch (mapView->selectedItemType())
        {
        case Edge::Type:
        {
            setEdgeTabEnabled(true);
            tabWidget->setCurrentWidget(pathTab);
            Edge* e = mapView->selectedEdge();
            timeSpinBox->setValue(e->cost());

            safeCheck->setChecked(e->valid());

            selectedEdge = e;
            break;
        }
        case Node::Type:
        {
            setNodeTabEnabled(true);
            tabWidget->setCurrentWidget(planetTab);
            selectedNode = mapView->selectedNode();
            planetNameText->setText(selectedNode->name());
            xText->setText(QString::number(selectedNode->x()));
            yText->setText(QString::number(selectedNode->y()));

            QString s = QString("background-color: rgb(%1, %2, %3);")
                        .arg(selectedNode->color().red())
                        .arg(selectedNode->color().green())
                        .arg(selectedNode->color().blue());
            colorSelectionButton->setStyleSheet(s);
            colorSelectionButton->setAutoFillBackground(true);
            colorSelectionButton->update();

            break;
        }
        }
    }
    else
    {
        setEdgeTabEnabled(false);
    }
}

void MapMainWindow::selectColor()
{
    if (!selectedNode) return;

    QColor c = QColorDialog::getColor(selectedNode->color(),
                                      this,
                                      "Choose planet color",
                                      QColorDialog::DontUseNativeDialog);

    selectedNode->setColor(c);
    selectedNode->update();

    QString s = QString("background-color: rgb(%1, %2, %3);")
                .arg(c.red())
                .arg(c.green())
                .arg(c.blue());
    colorSelectionButton->setStyleSheet(s);
    colorSelectionButton->setAutoFillBackground(true);
    colorSelectionButton->update();
}

void MapMainWindow::changeTime(int i)
{
    if (!selectedEdge) return;

    selectedEdge->setCost(i);
}

void MapMainWindow::setPathValid(bool checked)
{
    if (!selectedEdge) return;

    selectedEdge->setValid(checked);
}

void MapMainWindow::buildGridIndex()
{
    auto nodes = graph.nodes();
    grid = new GridIndex<Node*, int>(nodes);
    QGraphicsScene* scene = mapView->scene();
    QSize size = mapView->sceneSize();
    scene->addItem(new GridIndexItem(grid, size.width(), size.height()));
}

void MapMainWindow::showGridSearch()
{
    if (grid)
    {
        GridSearchDialog* searchDialog = new GridSearchDialog(grid);
        searchDialog->exec();
        delete searchDialog;
    }
}

void MapMainWindow::setup()
{
    resize(800, 600);

    menuBar = new QMenuBar(this);
    menuBar->setObjectName("menuBar");
    this->setMenuBar(menuBar);

    QMenu* fileMenu = menuBar->addMenu("File");
    openFileAction = fileMenu->addAction("&Open File");
    saveFileAction = fileMenu->addAction("&Save File");
    fileMenu->addSeparator();
    printAction = fileMenu->addAction("&Print...");
    printPreviewAction = fileMenu->addAction("Print Previe&w...");

    QMenu* toolsMenu = menuBar->addMenu("Tools");
    computePathsToAction = toolsMenu->addAction("Compute paths &to");
    computePathsFromAction = toolsMenu->addAction("Compute paths &from");
    buildGridIndexAction = toolsMenu->addAction("&Build grid index");
    showGridSearchDialog = toolsMenu->addAction("&Search in grid");

    toolBar = new QToolBar(this);
    toolBar->setObjectName("toolBar");
    this->addToolBar(toolBar);

    centralWidget = new QWidget(this);
    centralWidget->setObjectName("centralWidget");
    this->setCentralWidget(centralWidget);

    layout = new QHBoxLayout(centralWidget);
    layout->setObjectName("horizontalLayout");
    centralWidget->setLayout(layout);

    splitter = new QSplitter(centralWidget);
    splitter->setObjectName("splitter");
    splitter->setOrientation(Qt::Horizontal);

    layout->addWidget(splitter);

    //left sidebar
    tabWidget = new QTabWidget(splitter);
    tabWidget->setObjectName(QStringLiteral("tabWidget"));
    tabWidget->setGeometry(QRect(9, 9, 226, 191));
    tabWidget->setTabShape(QTabWidget::Rounded);
    tabWidget->setElideMode(Qt::ElideNone);
    tabWidget->setDocumentMode(false);
    tabWidget->setTabsClosable(false);
    tabWidget->setMovable(false);
    planetTab = new QWidget();
    planetTab->setObjectName(QStringLiteral("planetTab"));
    formLayout = new QFormLayout(planetTab);
    formLayout->setObjectName(QStringLiteral("formLayout"));
    planetNameLabel = new QLabel(planetTab);
    planetNameLabel->setObjectName(QStringLiteral("planetNameLabel"));

    formLayout->setWidget(0, QFormLayout::LabelRole, planetNameLabel);

    planetNameText = new QLineEdit(planetTab);
    planetNameText->setObjectName(QStringLiteral("planetNameText"));
    planetNameText->setReadOnly(true);

    formLayout->setWidget(0, QFormLayout::FieldRole, planetNameText);

    planetColorLabel = new QLabel(planetTab);
    planetColorLabel->setObjectName(QStringLiteral("planetColorLabel"));

    formLayout->setWidget(1, QFormLayout::LabelRole, planetColorLabel);

    colorSelectionButton = new QPushButton(planetTab);
    colorSelectionButton->setObjectName(QStringLiteral("colorSelectionButton"));

    formLayout->setWidget(1, QFormLayout::FieldRole, colorSelectionButton);

    verticalSpacer = new QSpacerItem(20, 20, QSizePolicy::Minimum, QSizePolicy::Expanding);

    formLayout->setItem(2, QFormLayout::LabelRole, verticalSpacer);

    coordinatesLabel = new QLabel(planetTab);
    coordinatesLabel->setObjectName(QStringLiteral("coordinatesLabel"));

    formLayout->setWidget(3, QFormLayout::LabelRole, coordinatesLabel);

    xLabel = new QLabel(planetTab);
    xLabel->setObjectName(QStringLiteral("xLabel"));

    formLayout->setWidget(4, QFormLayout::LabelRole, xLabel);

    yLabel = new QLabel(planetTab);
    yLabel->setObjectName(QStringLiteral("yLabel"));

    formLayout->setWidget(5, QFormLayout::LabelRole, yLabel);

    xText = new QLineEdit(planetTab);
    xText->setObjectName(QStringLiteral("xText"));
    xText->setReadOnly(true);

    formLayout->setWidget(4, QFormLayout::FieldRole, xText);

    yText = new QLineEdit(planetTab);
    yText->setObjectName(QStringLiteral("yText"));
    yText->setReadOnly(true);

    formLayout->setWidget(5, QFormLayout::FieldRole, yText);

    formLayout->setItem(6, QFormLayout::LabelRole, new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding));
    QLabel* vader = new QLabel();
    vader->setPixmap(QPixmap(":/SpaceMap/Resources/vader.png").scaled(128,128));
    formLayout->setWidget(7, QFormLayout::LabelRole, vader);

    tabWidget->addTab(planetTab, QString());
    pathTab = new QWidget();
    pathTab->setObjectName(QStringLiteral("pathTab"));
    formLayout_3 = new QFormLayout(pathTab);
    formLayout_3->setObjectName(QStringLiteral("formLayout_3"));
    travelTimeLabel = new QLabel(pathTab);
    travelTimeLabel->setObjectName(QStringLiteral("travelTimeLabel"));

    formLayout_3->setWidget(0, QFormLayout::LabelRole, travelTimeLabel);

    timeSpinBox = new QSpinBox(pathTab);
    timeSpinBox->setObjectName(QStringLiteral("timeSpinBox"));
    timeSpinBox->setMinimum(1);
    timeSpinBox->setMaximum(1000);

    formLayout_3->setWidget(0, QFormLayout::FieldRole, timeSpinBox);

    safeLabel = new QLabel(pathTab);
    safeLabel->setObjectName(QStringLiteral("safeLabel"));

    formLayout_3->setWidget(1, QFormLayout::LabelRole, safeLabel);

    safeCheck = new QCheckBox(pathTab);
    safeCheck->setCheckable(true);
    formLayout_3->setWidget(1, QFormLayout::FieldRole, safeCheck);

    tabWidget->addTab(pathTab, QString());

    splitter->addWidget(tabWidget);

    mapView = new MapView(splitter, this);
    mapView->setObjectName("mapView");
    splitter->addWidget(mapView);

    connect(mapView, &MapView::nodeAdded,
            this, &MapMainWindow::addNode);
    connect(mapView, &MapView::edgeAdded,
            this, &MapMainWindow::addEdge);

    connect(openFileAction, &QAction::triggered,
            this, &MapMainWindow::openFile);
    connect(saveFileAction, &QAction::triggered,
            this, &MapMainWindow::saveFile);

    connect(computePathsToAction, &QAction::triggered,
            this, &MapMainWindow::computePathsTo);
    connect(computePathsFromAction, &QAction::triggered,
            this, &MapMainWindow::computePathsFrom);
    connect(buildGridIndexAction, &QAction::triggered,
            this, &MapMainWindow::buildGridIndex);

    connect(printAction, &QAction::triggered,
            this, &MapMainWindow::printDocument);
    connect(printPreviewAction, &QAction::triggered,
            this, &MapMainWindow::printPreview);

    connect(showGridSearchDialog, &QAction::triggered,
            this, &MapMainWindow::showGridSearch);

    connect(mapView->scene(), &QGraphicsScene::selectionChanged,
            this, &MapMainWindow::selectionChanged);

    connect(colorSelectionButton, &QPushButton::clicked,
            this, &MapMainWindow::selectColor);

    connect(timeSpinBox, SIGNAL(valueChanged(int)),
            this, SLOT(changeTime(int)));
    connect(safeCheck, &QCheckBox::toggled,
            this, &MapMainWindow::setPathValid);

    setEdgeTabEnabled(false);
    setNodeTabEnabled(false);

    retranslate();
}

void MapMainWindow::setEdgeTabEnabled(bool val)
{
    timeSpinBox->setEnabled(val);
    safeCheck->setEnabled(val);
}

void MapMainWindow::setNodeTabEnabled(bool val)
{
    xText->setEnabled(val);
    yText->setEnabled(val);
    planetNameText->setEnabled(val);
    colorSelectionButton->setEnabled(val);
}
