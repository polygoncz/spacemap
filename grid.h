/*! \file */
#pragma once

#include <array>
#include <cassert>
#include <limits>
#include <map>
#include <vector>

/*!
 * Spatial grid index is helper data structure for fast node access
 * by it's coordinate in space.
 *
 * Building grid index is implemented building sparse matrix with
 * `std::map` objects. When all nodes is in sparse matrix,
 * flat 2D array is created and sparse matrix is convert into them.
 * 2D flat array is advantageous for fast searching by interval index.
 *
 * Complexicity
 * ------------
 * Variable `x` is count of interval in x axis and `y` is
 *
 * Operation               | Complexicity
 * ---------               | ------------
 * get node by coordinates | \f$ O(x + y) \f$
 * get nodes in range      | \f$ O(2*(x + y)) \f$
 *
 * \tparam Type type of node, must be a pointer
 * \tparam Coordinate data type of coordinate
 * \tparam GetX function for get x coordinate from node. Signature: `Coordinate x<Type>(Type n)`
 * \tparam GetY function for get y coordinate from node. Signature: `Coordinate y<Type>(Type n)`
 * \tparam Comparer function for compare two coordinates. Return 0 when equals, less then 0
 *                  when first is lower and greater then 0 when first is greater.
 *                  Signature: `int cmp<Coordinate>(const Coordinate& first, const Coordinate& second)`.
 */
template <typename Type,
          typename Coordinate,
          typename GetX = x<Type>,
          typename GetY = y<Type>,
          typename Comparer = cmp<Coordinate>>
class GridIndex
{
    /*!
     * Data type of sparse matrix.
     */
    typedef std::map<size_t, std::map<size_t, Type>> SparseMatrix;

    /*!
     * Data type of sparse matrix row.
     */
    typedef std::map<size_t, Type> SparseRow;

public:

    /*!
    * Enum for axis description instead using only integer.
    * Integer may be used to.
    * \sa switchAxis
    */
    enum Axis
    {
        X = 0,
        Y = 1
    };

    /*!
     * Make a grid index data structures from vector of objects.
     * At first is matrix created as sparse matrix and finally
     * it is converted into 2D array for faster access.
     * \param[in] nodes vector with data for creating grid index
     */
    GridIndex(std::vector<Type>& nodes);

    /*!
     * Deallocate whole object.
     */
    ~GridIndex();

    /*!
     * Get node with specific coordinates.
     * When node with coordinates do not find, method throw
     * exception.
     * \param[in] x x-coordinates in scene
     * \param[in] y y-coordinates in scene
     * \return node with specific coordinates or nullptr
     */
    Type get(const Coordinate x, const Coordinate y) const;

    /*!
     * Get nodes in specified rectangle in scene.
     * \param[in] x1 x-coordinate of left-top point
     * \param[in] y1 y-coordinate of left-top point
     * \param[in] x2 x-coordinate of right-bottom point
     * \param[in] y2 y-coordinate of right-bottom point
     * \return vector of nodes in rectangle
     */
    std::vector<Type> get(Coordinate x1, Coordinate y1,
                          Coordinate x2, Coordinate y2) const;

    /*!
     * Get nodes in specified rectangle in scene.
     * \param[in] coords coordinates in array like (left-top x, left-top y,
     *            right-bottom x, right-bottom y)
     * \return vector of nodes in rectangle
     */
    std::vector<Type> get(Coordinate* coords) const;

    /*!
     * Return intervals in specified axis.
     * \param[in] axis specified axis
     * \return intervals in specified axis
     * \sa _intervals, Axis
     */
    std::vector<Coordinate> intervals(Axis a) const
    {
        return _intervals[a];
    }

private:

    /*!
     * Clean unused space in intervals vectors.
     */
    void shrinkIntervals();

    /*!
     * Get cell's coordinates where node lays.
     * \param[in] node node for searching
     * \return array of cell coordinates `{x, y}`
     */
    std::array<size_t, 2> getCell(Type node) const;

    /*!
    * Get cell's coordinates based on parameters `x` and `y`.
    * \param[in] x x-coordinates
    * \param[in] y y-coordinates
    * \return array of cell coordinates `{x, y}`
    */
    std::array<size_t, 2> getCell(const Coordinate& x, const Coordinate& y) const;

    /*!
     * Increase index of all rows its index is greater then
     * inserted index.
     * \param[out] sm sparse matrix
     * \param[in] cell index of row
     */
    void addRowBeforeIndex(SparseMatrix& sm, size_t cell);

    /*!
    * Increase index of all columns its index is greater then
    * inserted index.
    * \param[out] sm sparse matrix
    * \param[in] cell index of column
    */
    void addColumnBeforeIndex(SparseMatrix& sm, size_t cell);

    /*!
     * Check correctness of node placement in specified row.
     * \param[in] sm sparse matrix
     * \param[in] origIndex checked row
     */
    void reorganizeRows(SparseMatrix& sm, size_t origIndex);

    /*!
    * Check correctness of node placement in specified column.
    * \param[in] sm sparse matrix
    * \param[in] origIndex checked column
    */
    void reorganizeColumns(SparseMatrix& sm, size_t origIndex);

    /*!
     * Transform sparse matrix to one dimensional array.
     * \param[in] sm sparse matrix
     */
    void bakeMatrix(const SparseMatrix& sm);

    /*!
     * Method returns switched axis. If axis in argument is `Axis::X`
     * resp. `Axis::Y` returns `Axis::Y` resp. `Axis::X.
     * \param[in] axis axis for switching
     * \return switched axis
     * \sa Axis
     */
    inline Axis switchAxis(Axis axis) const
    {
        return (axis == X) ? Y : X;
    }

    /*!
     * Get coordinate in specified axis for node.
     * \param[in] node node for which search coordinate
     * \param[in] axis axis in which search coordinate
     * \return coordinate of node in specified axis
     */
    inline Coordinate getCoordinate(Type node, Axis axis) const
    {
        GetX x;
        GetY y;
        return (axis == Axis::X) ? x(node) : y(node);
    }

    /*!
     * Get both coordinates of node.
     * \param[in] node node for which getting coordinates
     * \return array of both coordinates `{x, y}`
     */
    inline std::array<Coordinate, 2> getCoordinates(Type node) const
    {
        GetX x;
        GetY y;
        return { x(node), y(node) };
    }

    /*!
     * Because matrix for searching is represented like 1D array,
     * this method compute the offset for get cell on specified
     * row and column.
     * \param[in] x row
     * \param[in] y collumn
     * \return address in matrix array
     * \sa matrix
     */
    inline size_t offset(size_t x, size_t y) const
    {
        return x * _intervals[1].size() + y;
    }

    /*!
     * Array of intervals for both axis.
     */
    std::vector<Coordinate> _intervals[2];

    /*!
     * Matrix represented like 1D array.
     */
    Type* matrix;
};

template <typename Type,
          typename Coordinate,
          typename GetX,
          typename GetY,
          typename Comparer>
GridIndex<Type, Coordinate, GetX, GetY, Comparer>::GridIndex(std::vector<Type>& nodes)
{
    SparseMatrix sparseMatrix;

    //insert low bounds
    _intervals[0].push_back(0);
    _intervals[1].push_back(0);

    Axis axis = X;

    //iterate over nodes
    for (Type node : nodes)
    {
        std::array<size_t, 2> cell = getCell(node);

        bool cellExist[2] = { false, false };
        cellExist[0] = (sparseMatrix.count(cell[0]) == 1);
        cellExist[1] = cellExist[0] ? (sparseMatrix.at(cell[0]).count(cell[1]) == 1) : false;

        if (cellExist[0] && cellExist[1])
        {
            Type existNode = sparseMatrix.at(cell[0]).at(cell[1]);

            Coordinate actualCoordinate = getCoordinate(node, axis);
            Coordinate existCoordinate = getCoordinate(existNode, axis);

            Coordinate diff = existCoordinate - actualCoordinate;

            Coordinate newIntervalBorder = static_cast<Coordinate>(actualCoordinate + diff / 2.0);
            _intervals[axis].push_back(newIntervalBorder);
            std::sort(_intervals[axis].begin(), _intervals[axis].end());

            if (axis == X)
            {
                if (diff <= 0)
                {
                    addRowBeforeIndex(sparseMatrix, cell[0] + 1);
                    reorganizeRows(sparseMatrix, cell[0]);
                    sparseMatrix[cell[0] + 1][cell[1]] = node;
                }
                else
                {
                    addRowBeforeIndex(sparseMatrix, cell[0]);
                    reorganizeRows(sparseMatrix, cell[0] + 1);
                    sparseMatrix[cell[0]][cell[1]] = node;
                }
            }
            else
            {
                if (diff <= 0)
                {
                    addColumnBeforeIndex(sparseMatrix, cell[1] + 1);
                    reorganizeColumns(sparseMatrix, cell[1]);
                    sparseMatrix[cell[0]][cell[1] + 1] = node;
                }
                else
                {
                    addColumnBeforeIndex(sparseMatrix, cell[1]);
                    reorganizeColumns(sparseMatrix, cell[1] + 1);
                    sparseMatrix[cell[0]][cell[1]] = node;
                }
            }

            axis = switchAxis(axis);
        }
        else
        {
            sparseMatrix[cell[0]][cell[1]] = node;
        }
    }

    bakeMatrix(sparseMatrix);
    shrinkIntervals();
}


template <typename Type,
          typename Coordinate,
          typename GetX /*= x<Type>*/,
          typename GetY /*= y<Type>*/,
          typename Comparer /*= cmp<Coordinate>*/>
void
GridIndex<Type, Coordinate, GetX, GetY, Comparer>::addRowBeforeIndex(SparseMatrix& sm, size_t cell)
{
    for (SparseMatrix::reverse_iterator it = sm.rbegin(); it != sm.rend(); ++it)
    {
        size_t index = it->first;
        if (index >= cell)
        {
            sm[index + 1] = sm[index];
            auto old = sm.find(index);
            sm.erase(old);
        }
        else
        {
            return;
        }
    }
}


template <typename Type,
          typename Coordinate,
          typename GetX /*= x<Type>*/,
          typename GetY /*= y<Type>*/,
          typename Comparer /*= cmp<Coordinate>*/>
void
GridIndex<Type, Coordinate, GetX, GetY, Comparer>::addColumnBeforeIndex(SparseMatrix& sm, size_t cell)
{
    for (SparseMatrix::iterator it1 = sm.begin(); it1 != sm.end(); ++it1)
    {
        size_t rowIndex = it1->first;
        SparseRow row = it1->second;

        for (SparseRow::reverse_iterator it2 = row.rbegin(); it2 != row.rend(); ++it2)
        {
            size_t columnIndex = it2->first;
            if (columnIndex >= cell)
            {
                sm[rowIndex][columnIndex + 1] = sm.at(rowIndex).at(columnIndex);
                auto old = sm.at(rowIndex).find(columnIndex);
                sm.at(rowIndex).erase(old);
            }
            else
            {
                break;
            }
        }
    }
}

template <typename Type,
          typename Coordinate,
          typename GetX /*= x<Type>*/,
          typename GetY /*= y<Type>*/,
          typename Comparer /*= cmp<Coordinate>*/>
void
GridIndex<Type, Coordinate, GetX, GetY, Comparer>::reorganizeRows(SparseMatrix& sm, size_t origIndex)
{
    SparseRow& origRow = sm.at(origIndex);

    if (origRow.size() != 0)
    {
        for (SparseRow::iterator it = origRow.begin(); it != origRow.end();)
        {
            size_t index = it->first;
            Type node = it->second;
            std::array<size_t, 2> cell = getCell(node);

            if (cell[0] != origIndex)
            {
                sm[cell[0]][index] = sm.at(origIndex).at(index); //move
                auto old = origRow.find(index);
                origRow.erase(it++);

                if (origRow.size() == 0)
                {
                    auto emptyRow = sm.find(origIndex);
                    sm.erase(emptyRow);
                    break;
                }
            }
            else
            {
                it++;
            }
        }
    }
}


template <typename Type,
          typename Coordinate,
          typename GetX /*= x<Type>*/,
          typename GetY /*= y<Type>*/,
          typename Comparer /*= cmp<Coordinate>*/>
void GridIndex<Type, Coordinate, GetX, GetY, Comparer>::reorganizeColumns(SparseMatrix& sm, size_t origIndex)
{
    for (SparseMatrix::iterator it = sm.begin(); it != sm.end(); it++)
    {
        size_t rowIndex = it->first;
        SparseRow& row = it->second;

        SparseRow::iterator nodeIter = row.find(origIndex);
        if (nodeIter != row.end())
        {
            size_t columnIndex = it->first;
            Type node = nodeIter->second;
            std::array<size_t, 2> cell = getCell(node);

            if (cell[1] != origIndex)
            {
                sm[rowIndex][cell[1]] = sm.at(rowIndex).at(origIndex);
                row.erase(nodeIter);
            }
        }
    }
}

template <typename Type,
          typename Coordinate,
          typename GetX /*= x<Type>*/,
          typename GetY /*= y<Type>*/,
          typename Comparer /*= cmp<Coordinate>*/>
void
GridIndex<Type, Coordinate, GetX, GetY, Comparer>::bakeMatrix(const SparseMatrix& sm)
{
    size_t sizeX = _intervals[0].size();
    size_t sizeY = _intervals[1].size();
    size_t size = sizeX * sizeY;

    matrix = new Type[size];
    memset(matrix, 0, sizeof(Type)* size);

    for (SparseMatrix::const_iterator rowIt = sm.cbegin(); rowIt != sm.cend(); ++rowIt)
    {
        size_t x = rowIt->first;
        const SparseRow& row = rowIt->second;
        for (SparseRow::const_iterator colIt = row.cbegin(); colIt != row.cend(); ++colIt)
        {
            size_t y = colIt->first;
            Type n = colIt->second;

            matrix[offset(x, y)] = n;
        }
    }
}


template <typename Type,
          typename Coordinate,
          typename GetX /*= X<Type>*/,
          typename GetY /*= Y<Type>*/,
          typename Comparer /*= std::less<Coordinate>*/>
GridIndex<Type, Coordinate, GetX, GetY, Comparer>::~GridIndex()
{
    if (matrix)
        delete[] matrix;
}

template <typename Type,
          typename Coordinate,
          typename GetX /*= X<Type>*/,
          typename GetY /*= Y<Type>*/,
          typename Comparer /*= std::less<Coordinate>*/>
Type
GridIndex<Type, Coordinate, GetX, GetY, Comparer>::get(const Coordinate x, const Coordinate y) const
{
    std::array<size_t, 2> cell = getCell(x, y);

    Type node = matrix[offset(cell[0], cell[1])];
    if (node)
    {
        std::array<Coordinate, 2> coords = getCoordinates(node);
        Comparer cmp;
        if (cmp(x, coords[0]) == 0 && cmp(y, coords[1]) == 0)
            return node;
        else
            return nullptr;
    }

    return nullptr;
}

template <typename Type,
          typename Coordinate,
          typename GetX /*= X<Type>*/,
          typename GetY /*= Y<Type>*/,
          typename Comparer /*= std::less<Coordinate>*/>
std::vector<Type>
GridIndex<Type, Coordinate, GetX, GetY, Comparer>::get(Coordinate x1, Coordinate y1, Coordinate x2, Coordinate y2) const
{
    if (x1 > x2) std::swap(x1, x2);
    if (y1 > y2) std::swap(y1, y2);

    std::vector<Type> out;

    std::array<size_t, 2> from = getCell(x1, y1);
    std::array<size_t, 2> to = getCell(x2, y2);

    for (size_t x = from[0]; x <= to[0]; ++x)
    {
        for (size_t y = from[1]; y <= to[1]; ++y)
        {
            Type n = matrix[offset(x, y)];
            if (n)
            {
                if (x == from[0] || x == to[0] || y == from[1] || y == to[1])
                {
                    std::array<Coordinate, 2> coords = getCoordinates(n);
                    if (x1 <= coords[0] && coords[0] < x2
                            && y1 <= coords[1] && coords[1] < y2)
                        out.push_back(n);
                }
                else
                {
                    out.push_back(n);
                }
            }
        }
    }

    return out;
}

template <typename Type,
          typename Coordinate,
          typename GetX /*= X<Type>*/,
          typename GetY /*= Y<Type>*/,
          typename Comparer /*= std::less<Coordinate>*/>
std::vector<Type>
GridIndex<Type, Coordinate, GetX, GetY, Comparer>::get(Coordinate* coords) const
{
    return get(coords[0], coords[1], coords[2], coords[3]);
}

template <typename Type,
          typename Coordinate,
          typename GetX /*= x<Type>*/,
          typename GetY /*= y<Type>*/,
          typename Comparer /*= cmp<Coordinate>*/>
std::array<size_t, 2>
GridIndex<Type, Coordinate, GetX, GetY, Comparer>::getCell(Type node) const
{
    return getCell(getCoordinate(node, Axis::X), getCoordinate(node, Axis::Y));
}

template <typename Type,
          typename Coordinate,
          typename GetX /*= x<Type>*/,
          typename GetY /*= y<Type>*/,
          typename Comparer /*= cmp<Coordinate>*/>
std::array<size_t, 2>
GridIndex<Type, Coordinate, GetX, GetY, Comparer>::getCell(const Coordinate& x, const Coordinate& y) const
{
    if (_intervals[0].size() == 1 && _intervals[1].size() == 1)
        return{ 0, 0 };

    Coordinate coords[2];
    coords[0] = x;
    coords[1] = y;

    Comparer comp;

    size_t cell[2];
    for (int axis = 0; axis < 2; ++axis)   // in every axis
    {
        if (_intervals[axis].size() == 1)
        {
            cell[axis] = 0;
            continue;
        }

        for (size_t i = 1; i < _intervals[axis].size(); ++i)   //find lower interval bounds index
        {
            bool isLessThenUpper = comp(coords[axis], _intervals[axis][i]) < 0;
            bool isGreaterThenLower = comp(coords[axis], _intervals[axis][i - 1]) >= 0;
            if (isLessThenUpper && isGreaterThenLower)
            {
                cell[axis] = i - 1;
                break;
            }

            if (i == _intervals[axis].size() - 1)
                cell[axis] = i;
        }
    }

    return { cell[0], cell[1] };
}

template <typename Type,
          typename Coordinate,
          typename GetX /*= x<Type>*/,
          typename GetY /*= y<Type>*/,
          typename Comparer /*= cmp<Coordinate>*/>
void
GridIndex<Type, Coordinate, GetX, GetY, Comparer>::shrinkIntervals()
{
    _intervals[0].shrink_to_fit();
    _intervals[1].shrink_to_fit();
}