#pragma once

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QFrame>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPlainTextEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QSplitter>
#include <QtWidgets/QVBoxLayout>

#include "grid.h"
#include "graphics.h"

class GridSearchDialog : public QDialog
{
    Q_OBJECT;

public:
    GridSearchDialog(GridIndex<Node*, int>* grid);

private slots:
    void nodeSearchAction();
    void intervalSearchAction();

private:
    void setupUi();
    void retranslateUi();

    GridIndex<Node*, int>* grid;

    QVBoxLayout *verticalLayout;
    QSplitter *splitter;
    QFrame *frame;
    QHBoxLayout *horizontalLayout;
    QGroupBox *groupBox;
    QFormLayout *formLayout;
    QLabel *label;
    QSpinBox *xNodeSpinBox;
    QLabel *label_2;
    QSpinBox *yNodeSpinBox;
    QPushButton *nodeSearch;
    QGroupBox *groupBox_2;
    QFormLayout *formLayout_2;
    QLabel *label_3;
    QSpinBox *x1IntervalSpinBox;
    QLabel *label_4;
    QSpinBox *x2IntervalSpinBox;
    QLabel *label_5;
    QSpinBox *y1IntervalSpinBox;
    QLabel *label_6;
    QSpinBox *y2IntervalSpinBox;
    QPushButton *intervalSearch;
    QPlainTextEdit *output;

};