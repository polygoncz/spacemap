var indexSectionsWithContent =
{
  0: "_abcdefgilmnoprstvxy~",
  1: "cdeglmnxy",
  2: "g",
  3: "abcdefgimnoprstv~",
  4: "_efgmnt",
  5: "ens",
  6: "a",
  7: "bfnt",
  8: "o",
  9: "s"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "typedefs",
  6: "enums",
  7: "enumvalues",
  8: "related",
  9: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Files",
  3: "Functions",
  4: "Variables",
  5: "Typedefs",
  6: "Enumerations",
  7: "Enumerator",
  8: "Friends",
  9: "Pages"
};

