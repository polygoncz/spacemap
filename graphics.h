#pragma once

#include <QGraphicsItem>
#include "grid.h"

class Node;
class Edge;

/*!
 * \brief Class represents node in graph.
 *
 * Node object can paint itself into QGraphicsView.
 * This node represents planet. Node contains list of edges
 * for better interacting with incident edges when painting.
 * It do not supply Graph structure.
 */
class Node : public QGraphicsItem
{
public:
    /*!
     * Create node with specific planet name.
     * \param[in] name planet name
     */
    explicit Node(const QString& name);

    /*!
     * Add incident edge into list for interactive rendering.
     * \param[in] e incident edge
     * \sa edges()
     */
    void addEdge(Edge* e);

    /*!
     * Returns QList of incident edges.
     * \return QList of incident edges
     * \sa addEdge()
     */
    QList<Edge *> edges(void) const;

    /*!
     * Return unique name of planet as const QString&.
     * \return planet name
     */
    const QString& name(void) const;

    /*!
     * Enum which is usable for detect data type for casting
     * with qgraphicsitem_cast. Type must be unique.
     * \sa type()
     */
    enum
    {
        Type = UserType + 1 //!< unique value for Node type
    };

    /*!
     * Returns value of enum Type. It suitable for
     * qgraphicsitem_cast function. Override method from QGraphicsItem.
     * \return value of type which unique for this type of object
     */
    int type() const override
    {
        return Type;
    }

    /*!
     * Return base color of planet. It is used for compute gradient.
     * \return base color of planet
     * \sa setColor()
     */
    QColor color(void) const;

    /*!
     * Set base color of planet. It is used for compute gradient.
     * \param[in] color base color
     * \sa color()
     */
    void setColor(const QColor& color);

    /*!
     * Provide bounding rectangular in scene coordinates for fast repainting.
     * Rectangular is union of planet circle bounding and text bounding.
     * \return bounding rectangular in scene coordinates
     * \sa shape()
     */
    QRectF boundingRect() const override;

    /*!
     * Provide path for selecting objects without massive overlaps which have
     * bounding rectangular.
     * \return shape of object like QPainterPath for precise selecting
     * \sa boundingRect()
     */
    QPainterPath shape() const override;

    /*!
     * Paint object on widget. Paint gradient circle with black stroke, shadow circle
     * and text with name of planet.
     * \param[in] painter painter object
     * \param[in] option options of graphics item (selected, mouse over, etc.)
     * \param[in] widget widget where paint object
     */
    void paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget) override;

    /*!
     * Serialize object into data stream.
     * \param[in] out data stream
     * \param[in] n node for serializing
     * \sa operator>>()
     */
    friend QDataStream& operator<<(QDataStream& out, const Node* n);

    /*!
    * Unserialize object from data stream.
    * \param[in] in data stream
    * \param[out] n node for unserializing
    * \sa operator<<()
    */
    friend QDataStream& operator>>(QDataStream& in, Node* n);

protected:
    /*!
     * This function is used for catch and process events which include
     * move or other changes of this node. It is used for recompute incident edges
     * painting coordinates and send signal for its repainting.
     * \param[in] change type of change
     * \param[in] value value of change
     * \return
     */
    QVariant itemChange(GraphicsItemChange change, const QVariant& value) override;

    /*!
     * Override of mouse press event. Add repainting for animate.
     */
    void mousePressEvent(QGraphicsSceneMouseEvent* event) override;
    /*!
    * Override of mouse release event. Add repainting for animate.
    */
    void mouseReleaseEvent(QGraphicsSceneMouseEvent* event) override;

private:
    /*!
     * Set name. Its only for unserializing object!
     * \param[in] n name
     */
    void setName(const QString& n);

    QList<Edge *> edgesList; //!< incident edges
    QString _name; //!< name of planet
    QColor _color; //!< base color
    QStaticText nameStatic; //!< prerendered text with name
};


/*!
 * \brief Class represented edge for Graph structure.
 *
 * Edge object can be paint in QGraphicsView component.
 * Edge carries information about cost of path and validity
 * of edge.
 *
 * Edge is painted like bold line with text with cost.
 * Also it can paint arrow by each node or both of them.
 * If edge is not valid it will be painted like dashed line.
 * If edge is selected it will be painted with red color.
 */
class Edge : public QGraphicsItem
{
public:
    /*!
     * Create edge object with mandatory from and to nodes and cost.
     * \param[in] from mandatory from node
     * \param[in] to mandatory to node
     * \param[in] cost cost of edge
     */
    Edge(Node* from, Node* to, int cost);

    /*!
    * Enum which is usable for detect data type for casting
    * with qgraphicsitem_cast. Type must be unique.
    * \sa type()
    */
    enum { Type = UserType + 2 };

    /*!
    * Returns value of enum Type. It suitable for
    * qgraphicsitem_cast function. Override method from QGraphicsItem.
    * \return value of type which unique for this type of object
    */
    int type() const override;

    /*!
     * Define which arrow is painted.
     * \sa setArrowType(), arrowType()
     */
    enum ArrowType
    {
        None = 1 << 0, //!< edges has no arrow
        From = 1 << 1, //!< arrow is drawn by from node
        To   = 1 << 2, //!< arrow is drawn by to node
        Both = From + To //!< arrow is drawn by both nodes
    };

    /*!
     * Return type of drawn arrow.
     * \return type of drawn arrow
     * \sa setArrowType(), ArrowType
     */
    ArrowType arrowType() const;

    /*!
     * Set type of drawn arrow.
     * \param[in] t type of drawn arrow
     * \sa arrowType(), ArrowType
     */
    void setArrowType(ArrowType t);

    /*!
     * Return from node.
     * \return from node
     * \sa to()
     */
    Node* from() const;

    /*!
     * Return to node.
     * \return to node
     * \sa from()
     */
    Node* to() const;

    /*!
     * Return if edge is valid for computing algorithm.
     * \return validity
     */
    bool valid(void) const;

    /*!
     * Set if edge is valid for computing algorithm.
     * \param[in] v validity
     */
    void setValid(bool v);

    /*!
     * Return cost of edge like integer number.
     * \return cost of edge
     */
    int cost(void) const;

    /*!
     * Set cost of edge.
     * \param[in] i new integer number of cost
     */
    void setCost(int i);

    /*!
     * Get color of edge.
     * \return color of edge
     */
    QColor color() const;

    /*!
     * Set color of edge
     * \param[in] c new color of edge
     */
    void setColor(const QColor& c);

    /*!
     * Recompute position of line from Node pointers from and to.
     * It is used when one of this node change its position.
     * \sa from(), to()
     */
    void adjust(void);

    /*!
     * Provide bounding rectangular in scene coordinates for fast repainting.
     * Rectangular is union of planet circle bounding and text bounding.
     * \return bounding rectangular in scene coordinates
     * \sa shape()
     */
    QRectF boundingRect() const override;

    /*!
     * Provide path for selecting objects without massive overlaps which have
     * bounding rectangular.
     * \return shape of object like QPainterPath for precise selecting
     * \sa boundingRect()
     */
    QPainterPath shape() const override;

    /*!
     * Paint object on widget. Paint line between two nodes from and to, text with cost
     * and arrows depend of setted ArrowType.
     * \param[in] painter painter object
     * \param[in] option options of graphics item (selected, mouse over, etc.)
     * \param[in] widget widget where paint object
     * \sa from(), to(), ArrowType, arrowType(), setArrowType()
     */
    void paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget) override;

    /*!
     * Serialize object into data stream. Unserializing must be done manualy,
     * with loaded nodes. This edges contains only name for searching correct Node object.
     * \param[in] out data stream
     * \param[in] e node for serializing
     */
    friend QDataStream& operator<<(QDataStream& out, const Edge* e);

private:
    Node *_from, //!< from node pointer
         *_to;   //!< to node pointer
    QPointF fromP, //!< from node coordinates
            toP; //!< to node coordinates
    QColor _color; //!< color of edge
    ArrowType _arrowType; //!< arrow type which is drawn
    bool _valid; //!< validity for algorithms
    int _cost; //!< cost of edge
};

/*!
 * \brief Specialezed std::less struct for Node pointers.
 * It used by Graph structure for compare value.
 *
 * Specialezed std::less struct for Node pointers.
 * It used by Graph structure for compare value.
 */
template<>
struct std::less<Node*>
{
    /*!
     * Do less compare between two nodes.
     */
    bool operator()(const Node* a, const Node* b) const
    {
        return a->name() < b->name();
    }
};

template<typename T>
struct x
{
    int operator()(T a) const
    {
        return 0;
    }
};

template<>
struct x<Node*>
{
    int operator()(Node* n) const
    {
        return qRound(n->x());
    }
};

template<typename T>
struct y
{
    int operator()(T a) const
    {
        return 0;
    }
};

template<>
struct y<Node*>
{
    int operator()(Node* n) const
    {
        return qRound(n->y());
    }
};

template<typename T>
struct cmp
{
    int operator()(T a, T b) const
    {
        return 0;
    }
};

template<>
struct cmp<int>
{
    int operator()(int a, int b) const
    {
        return a - b;
    }
};

class GridIndexItem : public QGraphicsItem
{
public:
    GridIndexItem(GridIndex<Node*, int>* grid, int width, int height);

    virtual QRectF boundingRect() const override;
    virtual void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget = 0) override;

private:
    GridIndex<Node*, int>* grid;
    int width, height;
};