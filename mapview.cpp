#include "stdafx.h"
#include "mapview.h"

#include "graphics.h"
#include "mapmainwindow.h"

const double MapView::stepScaleFactor = 1.2;

MapView::MapView(QWidget* parent, MapMainWindow* wnd)
    : QGraphicsView(parent),
      fromNode(nullptr),
      creatingEdge(false)
{
    setRenderHints(QPainter::Antialiasing | QPainter::SmoothPixmapTransform);
    setDragMode(ScrollHandDrag);
    setViewportUpdateMode(BoundingRectViewportUpdate);
    setCacheMode(CacheBackground);
    _scene = new QGraphicsScene(this);
    _scene->setItemIndexMethod(QGraphicsScene::NoIndex);
    setScene(_scene);
    setMinimumSize(640, 480);
    setInteractive(true);
    setTransformationAnchor(AnchorUnderMouse);

    QPixmap space(":/SpaceMap/Resources/space.jpg");
    background = new QGraphicsPixmapItem(space);
    background->setFlag(QGraphicsItem::ItemIsSelectable, false);
    background->setFlag(QGraphicsItem::ItemIsMovable, false);
    background->setZValue(-2);
    background->setPos(QPointF(0.0, 0.0));

    QImage metalImg = QImage(":/SpaceMap/Resources/metal.jpg");
    QBrush metal = QBrush(metalImg);
    setBackgroundBrush(metal);

    QSize s = space.size();
    _scene->setSceneRect(0, 0, s.width(), s.height());

    _scene->addItem(background);

    mainwindow = wnd;
}

MapView::~MapView()
{
}

#ifndef QT_NO_WHEELEVENT
void MapView::wheelEvent(QWheelEvent* ev)
{
    if (ev->delta() > 0)
        zoomIn();
    else
        zoomOut();

    ev->accept();
}
#endif

void MapView::addNode(Node* n)
{
    nodes.push_back(n);
    _scene->addItem(n);
}

void MapView::addEdge(Edge* e)
{
    edges.push_back(e);
    _scene->addItem(e);
}

void MapView::clear()
{
    _scene->clear();

    QPixmap space(":/SpaceMap/Resources/space.jpg");
    background = new QGraphicsPixmapItem(space);
    background->setFlag(QGraphicsItem::ItemIsSelectable, false);
    background->setFlag(QGraphicsItem::ItemIsMovable, false);
    background->setZValue(-2);
    background->setPos(QPointF(0.0, 0.0));

    _scene->addItem(background);
}

Node* MapView::selectedNode() const
{
    QList<QGraphicsItem*> items = _scene->selectedItems();
    if (items.size() > 0)
        return qgraphicsitem_cast<Node*>(items.front());
    else
        return nullptr;
}

Edge* MapView::selectedEdge(void) const
{
    QList<QGraphicsItem*> items = _scene->selectedItems();
    if (items.size() > 0)
        return qgraphicsitem_cast<Edge*>(items.front());
    else
        return nullptr;
}

int MapView::selectedItemType(void) const
{
    return scene()->selectedItems().first()->type();
}

bool MapView::hasSelectedItem(void) const
{
    return scene()->selectedItems().size() != 0;
}

QSize MapView::sceneSize(void) const
{
    return QSize(background->pixmap().width(),
                 background->pixmap().height());
}

void MapView::mousePressEvent(QMouseEvent* ev)
{
    if (ev->button() == Qt::RightButton)
    {
        QPointF sceneCoord = mapToScene(ev->pos());

        if (_scene->sceneRect().contains(sceneCoord))
        {
            QString name = QInputDialog::getText(this,
                                                 "Planet name dialog",
                                                 "Planet name:");
            if (!name.isEmpty())
            {
                Node* n = new Node(name);
                n->setPos(sceneCoord);

                _scene->addItem(n);
                nodes.push_back(n);

                emit nodeAdded(n);
            }
            else
            {
                QMessageBox::warning(this,
                                     "Missing planet name!",
                                     "Planet name was not created.");
            }
        }
    }

    if (ev->button() == Qt::MiddleButton)
    {
        QGraphicsItem* f = itemAt(ev->pos());
        if (f && f->type() == Node::Type)
        {
            fromNode = qgraphicsitem_cast<Node *>(f);
            creatingEdge = true;
        }
    }

    QGraphicsView::mousePressEvent(ev);
    ev->accept();
}

void MapView::mouseReleaseEvent(QMouseEvent* event)
{
    if (creatingEdge)
    {
        QGraphicsItem* f = itemAt(event->pos());
        if (f && f->type() == Node::Type)
        {
            int value = QInputDialog::getInt(this,
                                             "Travel time dialog",
                                             "Travel time:", 10, 1);

            Node* to = qgraphicsitem_cast<Node *>(f);
            Edge* e = new Edge(fromNode, to, value);
            _scene->addItem(e);
            edges << e;

            emit edgeAdded(e);
        }

        fromNode = nullptr;
        creatingEdge = false;
    }

    QGraphicsView::mouseReleaseEvent(event);
    event->accept();
}

void MapView::zoomIn()
{
    scale(stepScaleFactor, stepScaleFactor);
    currentZoomFactor *= stepScaleFactor;
}

void MapView::zoomOut()
{
    scale(1 / stepScaleFactor, 1 / stepScaleFactor);
    currentZoomFactor /= stepScaleFactor;
}