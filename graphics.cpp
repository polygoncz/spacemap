#include "stdafx.h"
#include "graphics.h"

#include <cmath>

#include <QRadialGradient>
#include <QLineF>

double pi()
{
    return 3.141592653589793238463;
}

Node::Node(const QString& name)
    : _name(name)
{
    setFlag(ItemIsMovable);
    setFlag(ItemIsSelectable);
    setFlag(ItemSendsGeometryChanges);
    setCacheMode(DeviceCoordinateCache);
    setZValue(1);
    _color = QColor(qrand() % 150 + 100,
                    qrand() % 150 + 100,
                    qrand() % 150 + 100);

    nameStatic = QStaticText(name);
    nameStatic.prepare();
}

void Node::addEdge(Edge* e)
{
    edgesList << e;
}

QList<Edge*> Node::edges() const
{
    return edgesList;
}

const QString& Node::name(void) const
{
    return _name;
}

QColor Node::color(void) const
{
    return _color;
}

void Node::setColor(const QColor& color)
{
    _color = color;
    update();
}

QRectF Node::boundingRect() const
{
    qreal adjust = 3;
    return QRectF(-15 - adjust, -15 - adjust, 33 + adjust + nameStatic.size().width() + 50, 33 + adjust + nameStatic.size().height());
}

QPainterPath Node::shape() const
{
    QPainterPath path;
    path.addEllipse(-15, -15, 30, 30);
    return path;
}

void Node::paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget)
{
    painter->setPen(Qt::NoPen);
    painter->setBrush(QColor(Qt::darkGray).darker());
    painter->drawEllipse(-12, -12, 30, 30);

    painter->setPen(QPen(Qt::yellow, 2));
    painter->drawStaticText(18, -15, nameStatic);
    ::x<Node*> getX;
    ::y<Node*> getY;
    painter->drawText(18, 15, "{" + QString::number(getX(this)) + "; " + QString::number(getY(this)) + "}");

    QRadialGradient gradient(-7, -7, 15);
    if (option->state & QStyle::State_Sunken)
    {
        gradient.setColorAt(1, _color.lighter());
        gradient.setColorAt(0, _color.darker().darker());
    }
    else
    {
        gradient.setColorAt(0, _color.lighter());
        gradient.setColorAt(1, _color.darker());
    }

    painter->setBrush(gradient);

    if (option->state & QStyle::State_Selected)
        painter->setPen(QPen(Qt::red, 3));
    else
        painter->setPen(QPen(Qt::black, 3));

    painter->drawEllipse(-15, -15, 30, 30);

    painter->setPen(QPen(Qt::blue, 2));
    painter->drawLine(0, -5, 0, 5);
    painter->drawLine(-5, 0, 5, 0);
}

QVariant Node::itemChange(GraphicsItemChange change, const QVariant& value)
{
    switch (change)
    {
    case ItemPositionHasChanged:
        for (Edge* e : edges())
            e->adjust();
        break;
    case ItemPositionChange:
        if (scene())
        {
            QRectF rect = scene()->sceneRect();
            QPointF newPos = value.toPointF();
            if (!rect.contains(newPos))
            {
                newPos.setX(qMin(rect.right(), qMax(newPos.x(), rect.left())));
                newPos.setY(qMin(rect.bottom(), qMax(newPos.y(), rect.top())));
                return newPos;
            }
        }
        break;
    default:
        break;
    }

    return QGraphicsItem::itemChange(change, value);
}

void Node::mousePressEvent(QGraphicsSceneMouseEvent* event)
{
    update();
    QGraphicsItem::mousePressEvent(event);
}

void Node::mouseReleaseEvent(QGraphicsSceneMouseEvent* event)
{
    update();
    QGraphicsItem::mouseReleaseEvent(event);
}

void Node::setName(const QString& n)
{
    prepareGeometryChange();
    _name = n;
    nameStatic.setText(_name);
    nameStatic.prepare();
}

QDataStream& operator<<(QDataStream& out, const Node* n)
{
    if (!n) return out;

    out << n->_name << n->_color << n->pos();
    return out;
}

QDataStream& operator>>(QDataStream& in, Node* n)
{
    if (!n) return in;

    QString name;
    in >> name;
    n->setName(name);

    in >> n->_color;

    QPointF pos;
    in >> pos;

    n->setPos(pos);

    return in;
}

Edge::Edge(Node* from, Node* to, int cost)
    : _from(from), _to(to), _color(QColor(126, 202, 234)), _cost(cost), _arrowType(None)
{
    _from->addEdge(this);
    _to->addEdge(this);
    setZValue(0);
    //setFlag(QGraphicsItem::ItemIsSelectable);
    adjust();
}

int Edge::type() const
{
    return Type;
}

Edge::ArrowType Edge::arrowType() const
{
    return _arrowType;
}

void Edge::setArrowType(ArrowType t)
{
    _arrowType = t;
    update();
}

Node* Edge::from() const
{
    return _from;
}

Node* Edge::to() const
{
    return _to;
}

bool Edge::valid(void) const
{
    return _valid;
}

void Edge::setValid(bool v)
{
    _valid = v;
    update();
}

int Edge::cost() const
{
    return _cost;
}

void Edge::setCost(int i)
{
    _cost = i;
    update();
}

inline QColor Edge::color() const
{
    return _color;
}

void Edge::setColor(const QColor& c)
{
    _color = c;
    update();
}

void Edge::adjust()
{
    if (!_from || !_to)
        return;

    QLineF line(mapFromItem(_from, 0, 0), mapFromItem(_to, 0, 0));
    qreal length = line.length();

    prepareGeometryChange();

    if (length > qreal(30.0))
    {
        QPointF offset((line.dx() * 18) / length, (line.dy() * 18) / length);
        fromP = line.p1() + offset;
        toP = line.p2() - offset;
    }
    else
    {
        fromP = toP = line.p1();
    }
}

QRectF Edge::boundingRect() const
{
    if (!_from || !_to)
        return QRectF();

    qreal pen = 3;
    qreal extra = pen / 2.0;
    return QRectF(fromP, QSizeF(toP.x() - fromP.x(),
                                toP.y() - fromP.y()))
           .normalized()
           .adjusted(-extra, -extra, extra, extra);
}

QPainterPath Edge::shape() const
{
    QPainterPath path;
    path.addRect(boundingRect());
    return path;
}

void Edge::paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget)
{
    if (!_from || !_to)
        return;

    QLineF line(fromP, toP);
    if (qFuzzyCompare(line.length(), 0.0))
        return;

    QColor c = color();
    if (option->state & QStyle::State_Sunken)
        c = Qt::red;

    Qt::PenStyle penStyle = Qt::SolidLine;
    if (!valid())
    {
        penStyle = Qt::DashLine;
        c = c.darker();
    }

    painter->setPen(QPen(c, 3, penStyle, Qt::RoundCap));

    painter->drawLine(line);

    if (arrowType() & ArrowType::From)
    {
        double angle = acos(line.dx() / line.length());
        if (line.dy() > 0)
            angle = (2 * pi()) - angle;

        QPointF arrowP1 = line.p1() + QPointF(sin(angle + pi() / 3) * 10,
                                              cos(angle + pi() / 3) * 10);
        QPointF arrowP2 = line.p1() + QPointF(sin(angle + pi() - pi() / 3) * 10,
                                              cos(angle + pi() - pi() / 3) * 10);

        QPolygonF arrowhead;
        arrowhead << line.p1() << arrowP1 << arrowP2;
        painter->setBrush(QBrush(c));
        painter->drawPolygon(arrowhead);
    }

    if (arrowType() & ArrowType::To)
    {
        double angle = acos(line.dx() / line.length());
        if (line.dy() > 0)
            angle = (2 * pi()) - angle;

        QPointF arrowP1 = line.p2() - QPointF(sin(angle + pi() / 3) * 10,
                                              cos(angle + pi() / 3) * 10);
        QPointF arrowP2 = line.p2() - QPointF(sin(angle + pi() - pi() / 3) * 10,
                                              cos(angle + pi() - pi() / 3) * 10);

        QPolygonF arrowhead;
        arrowhead << line.p2() << arrowP1 << arrowP2;
        painter->setBrush(QBrush(c));
        painter->drawPolygon(arrowhead);
    }

    painter->setPen(QPen(Qt::yellow, 2));
    QStaticText staticCostText = QStaticText(QString::number(_cost));
    painter->drawStaticText(boundingRect().center(), staticCostText);
}

QDataStream& operator<<(QDataStream& out, const Edge* e)
{
    out << e->_cost << e->_from->name() << e->_to->name();
    return out;
}

GridIndexItem::GridIndexItem(GridIndex<Node*, int>* grid, int width, int height)
    : grid(grid), width(width), height(height)
{
    setFlag(ItemIsSelectable, false);
    setFlag(ItemIsMovable, false);
    setZValue(100);
}

QRectF GridIndexItem::boundingRect() const
{
    return QRectF(0, 0, width, height);
}

void GridIndexItem::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget /*= 0*/)
{
    painter->setPen(QPen(Qt::red, 2));
    painter->drawRect(QRectF(0, 0, width, height));

    painter->setPen(QPen(Qt::red, 1));
    std::vector<int> intervalsX = grid->intervals(GridIndex<Node*, int>::Axis::X);
    for (size_t i = 1; i < intervalsX.size(); ++i)
    {
        int x = intervalsX[i];
        painter->drawLine(x, 0, x, height);
    }

    std::vector<int> intervalsY = grid->intervals(GridIndex<Node*, int>::Axis::Y);
    for (size_t i = 1; i < intervalsY.size(); ++i)
    {
        int y = intervalsY[i];
        painter->drawLine(0, y , width, y);
    }
}
