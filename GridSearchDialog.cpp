#include "stdafx.h"
#include "GridSearchDialog.h"



GridSearchDialog::GridSearchDialog(GridIndex<Node*, int>* grid)
    : grid(grid)
{
    setupUi();
}

void GridSearchDialog::setupUi()
{
    if (objectName().isEmpty())
        setObjectName(QStringLiteral("GridSearch"));
    resize(523, 485);
    verticalLayout = new QVBoxLayout(this);
    verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
    splitter = new QSplitter(this);
    splitter->setObjectName(QStringLiteral("splitter"));
    splitter->setOrientation(Qt::Vertical);
    splitter->setOpaqueResize(true);
    splitter->setHandleWidth(5);
    splitter->setChildrenCollapsible(true);
    frame = new QFrame(splitter);
    frame->setObjectName(QStringLiteral("frame"));
    frame->setFrameShape(QFrame::StyledPanel);
    frame->setFrameShadow(QFrame::Raised);
    horizontalLayout = new QHBoxLayout(frame);
    horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
    groupBox = new QGroupBox(frame);
    groupBox->setObjectName(QStringLiteral("groupBox"));
    formLayout = new QFormLayout(groupBox);
    formLayout->setObjectName(QStringLiteral("formLayout"));
    label = new QLabel(groupBox);
    label->setObjectName(QStringLiteral("label"));

    formLayout->setWidget(0, QFormLayout::LabelRole, label);

    xNodeSpinBox = new QSpinBox(groupBox);
    xNodeSpinBox->setObjectName(QStringLiteral("xNodeSpinBox"));
    xNodeSpinBox->setMaximum(2000);

    formLayout->setWidget(0, QFormLayout::FieldRole, xNodeSpinBox);

    label_2 = new QLabel(groupBox);
    label_2->setObjectName(QStringLiteral("label_2"));

    formLayout->setWidget(1, QFormLayout::LabelRole, label_2);

    yNodeSpinBox = new QSpinBox(groupBox);
    yNodeSpinBox->setObjectName(QStringLiteral("yNodeSpinBox"));
    yNodeSpinBox->setMaximum(2000);

    formLayout->setWidget(1, QFormLayout::FieldRole, yNodeSpinBox);

    nodeSearch = new QPushButton(groupBox);
    nodeSearch->setObjectName(QStringLiteral("nodeSearch"));

    formLayout->setWidget(2, QFormLayout::FieldRole, nodeSearch);


    horizontalLayout->addWidget(groupBox);

    groupBox_2 = new QGroupBox(frame);
    groupBox_2->setObjectName(QStringLiteral("groupBox_2"));
    formLayout_2 = new QFormLayout(groupBox_2);
    formLayout_2->setObjectName(QStringLiteral("formLayout_2"));
    label_3 = new QLabel(groupBox_2);
    label_3->setObjectName(QStringLiteral("label_3"));

    formLayout_2->setWidget(0, QFormLayout::LabelRole, label_3);

    x1IntervalSpinBox = new QSpinBox(groupBox_2);
    x1IntervalSpinBox->setObjectName(QStringLiteral("x1IntervalSpinBox"));
    x1IntervalSpinBox->setMaximum(2000);

    formLayout_2->setWidget(0, QFormLayout::FieldRole, x1IntervalSpinBox);

    label_4 = new QLabel(groupBox_2);
    label_4->setObjectName(QStringLiteral("label_4"));

    formLayout_2->setWidget(1, QFormLayout::LabelRole, label_4);

    x2IntervalSpinBox = new QSpinBox(groupBox_2);
    x2IntervalSpinBox->setObjectName(QStringLiteral("x2IntervalSpinBox"));
    x2IntervalSpinBox->setMaximum(2000);

    formLayout_2->setWidget(1, QFormLayout::FieldRole, x2IntervalSpinBox);

    label_5 = new QLabel(groupBox_2);
    label_5->setObjectName(QStringLiteral("label_5"));

    formLayout_2->setWidget(2, QFormLayout::LabelRole, label_5);

    y1IntervalSpinBox = new QSpinBox(groupBox_2);
    y1IntervalSpinBox->setObjectName(QStringLiteral("y1IntervalSpinBox"));
    y1IntervalSpinBox->setMaximum(2000);

    formLayout_2->setWidget(2, QFormLayout::FieldRole, y1IntervalSpinBox);

    label_6 = new QLabel(groupBox_2);
    label_6->setObjectName(QStringLiteral("label_6"));

    formLayout_2->setWidget(3, QFormLayout::LabelRole, label_6);

    y2IntervalSpinBox = new QSpinBox(groupBox_2);
    y2IntervalSpinBox->setObjectName(QStringLiteral("y2IntervalSpinBox"));
    y2IntervalSpinBox->setMaximum(2000);

    formLayout_2->setWidget(3, QFormLayout::FieldRole, y2IntervalSpinBox);

    intervalSearch = new QPushButton(groupBox_2);
    intervalSearch->setObjectName(QStringLiteral("intervalSearch"));

    formLayout_2->setWidget(4, QFormLayout::FieldRole, intervalSearch);


    horizontalLayout->addWidget(groupBox_2);

    splitter->addWidget(frame);
    output = new QPlainTextEdit(splitter);
    output->setObjectName(QStringLiteral("output"));
    splitter->addWidget(output);

    verticalLayout->addWidget(splitter);

    connect(nodeSearch, &QPushButton::clicked,
            this, &GridSearchDialog::nodeSearchAction);
    connect(intervalSearch, &QPushButton::clicked,
            this, &GridSearchDialog::intervalSearchAction);

    output->setReadOnly(true);


    retranslateUi();
}

void GridSearchDialog::retranslateUi()
{
    setWindowTitle(QApplication::translate("GridSearch", "Dialog", 0));
    groupBox->setTitle(QApplication::translate("GridSearch", "Node search", 0));
    label->setText(QApplication::translate("GridSearch", "X coordinate:", 0));
    label_2->setText(QApplication::translate("GridSearch", "Y coordinate:", 0));
    nodeSearch->setText(QApplication::translate("GridSearch", "Search", 0));
    groupBox_2->setTitle(QApplication::translate("GridSearch", "Interval search", 0));
    label_3->setText(QApplication::translate("GridSearch", "X1 coordinate:", 0));
    label_4->setText(QApplication::translate("GridSearch", "X2 coordinate:", 0));
    label_5->setText(QApplication::translate("GridSearch", "Y1 coordinate:", 0));
    label_6->setText(QApplication::translate("GridSearch", "Y2 coordinate:", 0));
    intervalSearch->setText(QApplication::translate("GridSearch", "Search", 0));
}

void GridSearchDialog::nodeSearchAction()
{
    int xCoord = xNodeSpinBox->value();
    int yCoord = yNodeSpinBox->value();

    Node* n = grid->get(xCoord, yCoord);
    if (n)
        output->setPlainText(QString("%1 - (%2; %3)")
                             .arg(n->name(),
                                  QString::number(n->x()),
                                  QString::number(n->y())));
    else
        output->setPlainText("No node was found.");
}

void GridSearchDialog::intervalSearchAction()
{
    int x1 = x1IntervalSpinBox->value();
    int x2 = x2IntervalSpinBox->value();
    int y1 = y1IntervalSpinBox->value();
    int y2 = y2IntervalSpinBox->value();

    std::vector<Node*> nodes = grid->get(x1, y1, x2, y2);
    if (nodes.size() != 0)
    {
        QString out("");
        for (auto n : nodes)
            out += QString("%1 - (%2; %3)\n")
                   .arg(n->name(),
                        QString::number(n->x()),
                        QString::number(n->y()));
        output->setPlainText(out);
    }
    else
    {
        output->setPlainText("No nodes were found.");
    }
}