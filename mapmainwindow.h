#pragma once

#include <QMainWindow>

#include "mapview.h"
#include <QtPrintSupport/qprinter.h>

class MapMainWindow : public QMainWindow
{
    Q_OBJECT;
public:
    explicit MapMainWindow(QWidget* parent = nullptr, Qt::WindowFlags flags = 0);
    ~MapMainWindow();

    void retranslate();

private slots:
    void addNode(Node* n);
    void addEdge(Edge* e);
    void saveFile();
    void openFile();
    void computePathsTo();
    void computePathsFrom();

    void setAllEdges(const QColor& color, Edge::ArrowType arrow);

    void printDocument();
    void printPreview();
    void preparePreview(QPrinter* p);
    void selectionChanged();
    void selectColor();
    void changeTime(int i);
    void setPathValid(bool checked);

    void buildGridIndex();
    void showGridSearch();

public:
    QLineEdit* cityName;
    QLineEdit* edgeValue;

private:
    void setup();
    void setEdgeTabEnabled(bool val);
    void setNodeTabEnabled(bool val);
    QMenuBar* menuBar;
    QToolBar* toolBar;
    QSplitter* splitter;
    QWidget* centralWidget;
    QHBoxLayout* layout;
    QWidget* leftPanel;
    MapView* mapView;

    QAction* openFileAction;
    QAction* saveFileAction;
    QAction* computePathsToAction;
    QAction* printPreviewAction;
    QAction* printAction;
    QAction* computePathsFromAction;
    QAction* buildGridIndexAction;
    QAction* showGridSearchDialog;

    //left tab
    QTabWidget *tabWidget;
    QWidget *planetTab;
    QFormLayout *formLayout;
    QLabel *planetNameLabel;
    QLineEdit *planetNameText;
    QLabel *planetColorLabel;
    QPushButton *colorSelectionButton;
    QSpacerItem *verticalSpacer;
    QLabel *coordinatesLabel;
    QLabel *xLabel;
    QLabel *yLabel;
    QLineEdit *xText;
    QLineEdit *yText;
    QWidget *pathTab;
    QFormLayout *formLayout_3;
    QLabel *travelTimeLabel;
    QSpinBox *timeSpinBox;
    QLabel *safeLabel;
    QCheckBox *safeCheck;

    SpaceMapEditor::Structures::Graph<Node*, Edge*> graph;
    GridIndex<Node*, int>* grid;

    bool showComputeResult;
    Node* selectedNode;
    Edge* selectedEdge;
};