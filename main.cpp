#include "stdafx.h"
#include "mapmainwindow.h"
#include <QtWidgets/QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    qsrand(QTime::currentTime().msec());

    MapMainWindow main;
    main.show();

    return a.exec();
}
