/********************************************************************************
** Form generated from reading UI file 'spacemap.ui'
**
** Created by: Qt User Interface Compiler version 5.4.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SPACEMAP_H
#define UI_SPACEMAP_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_SpaceMapClass
{
public:
    QMenuBar *menuBar;
    QToolBar *mainToolBar;
    QWidget *centralWidget;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *SpaceMapClass)
    {
        if (SpaceMapClass->objectName().isEmpty())
            SpaceMapClass->setObjectName(QStringLiteral("SpaceMapClass"));
        SpaceMapClass->resize(600, 400);
        menuBar = new QMenuBar(SpaceMapClass);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        SpaceMapClass->setMenuBar(menuBar);
        mainToolBar = new QToolBar(SpaceMapClass);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        SpaceMapClass->addToolBar(mainToolBar);
        centralWidget = new QWidget(SpaceMapClass);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        SpaceMapClass->setCentralWidget(centralWidget);
        statusBar = new QStatusBar(SpaceMapClass);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        SpaceMapClass->setStatusBar(statusBar);

        retranslateUi(SpaceMapClass);

        QMetaObject::connectSlotsByName(SpaceMapClass);
    } // setupUi

    void retranslateUi(QMainWindow *SpaceMapClass)
    {
        SpaceMapClass->setWindowTitle(QApplication::translate("SpaceMapClass", "SpaceMap", 0));
    } // retranslateUi

};

namespace Ui {
    class SpaceMapClass: public Ui_SpaceMapClass {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SPACEMAP_H
