#pragma once

#include <map>

#include "graph.h"
#include "graphics.h"

/*!
 * \brief Function object of Dijkstra algorithm. It returns vector of predecessors.
 *
 * Complexicity is:
 *
 * \f$ O(|H|\log(|N|)) \f$
 *
 * Instead priority queue, where are problems with modify priority value
 * correctly and fast, there is used std::vector which is sorted by priority
 * on end of each iteration.
 */
class DijkstraToNode
{
public:
    /*!
     * Create functor for concrete graph and `to` node.
     * \param[in] graph Graph object
     * \param[in] to nodes for computing to paths with each other nodes in graph
     */
    DijkstraToNode(SpaceMapEditor::Structures::Graph<Node*, Edge*>* graph,
                   Node* to);

    /*!
     * Compute vector of predecessors, in `std::map` object.
     * \return vector of predecessors
     */
    std::map<Node*, Node*> operator()() const;

private:
    /*!
     * Find distance from node to its neighbors.
     */
    int findDistance(Node* node, std::vector<std::pair<Node*, int>> distances) const;

    SpaceMapEditor::Structures::Graph<Node*, Edge*>* graph; //!< graph
    Node *to; //!< `to` node
};

/*!
 * \brief Function object of Dijkstra algorithm. It return matrix of followers.
 *
 * It used DijkstraToNode algorithm and transform vector of predecessors to matrix of followers.
 */
class DijkstraFromNode
{
public:
    /*!
     * Create functor for concrete graph and `from` node.
     * \param[in] graph Graph object
     * \param[in] from nodes for computing paths with each other nodes in graph
     */
    DijkstraFromNode(SpaceMapEditor::Structures::Graph<Node*, Edge*>* graph,
                     Node* from);

    /*!
     * Compute matrix of followers. It is represented like sparse matrix
     * with two `std::map` objects.
     * \return matrix of followers
     */
    std::map<Node*, std::map<Node*, Node*>> operator()() const;

private:
    SpaceMapEditor::Structures::Graph<Node*, Edge*>* graph; //!< graph
    Node *from; //!< from node
};